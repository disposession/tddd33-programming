#include <iostream>

#include "list.h"

using namespace std;

#define SOLUTION 3 // Change to 1 when completely done

int main() {
#if SOLUTION == 0
  
    // Minimal code to use the list

    // You are recommended to start with your template implementation
    // and convert this code to instantiate it.
    // Move on to the iterator once it compiles.

    List<int> list;
    decltype(list)::value_type number;

    cout << "Enter numbers, finish by Ctrl-D " << endl;
    while (cin >> number) {
        list.insert(number);
    }
  
#elif SOLUTION == 1
    List<int> list;
    // The list is filled with data here...
    // ...
    list.insert(1);
    list.insert(2);
    list.insert(3);
    for ( List<int>::Iterator it = list.begin(); it != list.end(); ++it) {
        cout << *it << endl;
    }

    cout << list << endl;

#elif SOLUTION == 2
    List<string> word_list;
    word_list.insert("hi");
    word_list.insert("hello");
    word_list.insert("hej");
    // The list is filled with data here...
    // ...
    // Once the above work, try this
    for ( auto s : word_list ) {
        cout << s << endl;
    }
#else
  // example of using the list with integers
  {
    List<int> list;
    decltype(list)::value_type number;
    
    cout << "Enter numbers, finish by Ctrl-D " << endl;  
    while (cin >> number)
    {
      list.insert(number);
    }

    for ( decltype(list)::Iterator it{ list.begin() }; it != list.end(); ++it) {
      cout << *it << " ";
    }
    cout << endl;

    for ( auto i : list )
    {
      cout << i << " ";
    }
    cout << endl;

    cout << list << endl;
  }

  cin.clear();
  
  // example of using the list with std::string
  // ( notice the overwhelming similarity to above code? )
  {
    List<std::string> list;
    decltype(list)::value_type word;
    
    cout << "Enter words, finish by Ctrl-D " << endl;  
    while (cin >> word)
    {
      list.insert(word);
    }

    for ( decltype(list)::Iterator it{ list.begin() };
          it != list.end(); ++it)
    {
      cout << *it << " ";
    }
    cout << endl;
    
    for ( auto w : list )
    {
      cout << w << " ";
    }
    cout << endl;
    
    cout << list << endl;
  }
#endif
  
  return 0;
}
