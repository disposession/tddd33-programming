#ifndef _LIST_H_
#define _LIST_H_

#include <iostream>
#include <string>

using namespace std;

template <class T>
class List {
public:
    List();
    ~List() { delete first; }

    void insert(T const& d);

    List(List const&);
    List(List&&);
    List& operator=(List const&);
    List& operator=(List&&);


private:
    friend class Iterator;

    class Link {
    public:
        Link(T const& d, Link* n) : data(d), next(n) {}
        ~Link() { delete next; }

        friend class List;

        static Link* clone(Link const*);

    private:

        T data;
        Link* next;
    };

    Link* first;

public:

    using value_type = T;

    // Suitable place to add things...
    class Iterator {
    public:
        Iterator& operator ++ ();
        // Iterator& operator ++ (int );
        bool operator != (const Iterator& tracker) const;
        bool operator == (const Iterator& tracker) const;
        T& operator * () const;
        // T* operator -> () const;
        bool operator != (const Iterator& tracker) const;
        
        friend class List;

    private:
        Link* current;

        Iterator(Link* current) : current{current} {};
    };

    Iterator begin() const;
    Iterator end() const;
};

template <class T>
ostream& operator<< (ostream& os, const List<T>& list);

#include "list.tcc"
#endif
