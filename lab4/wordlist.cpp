#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <map>
#include <string>
#include <iomanip>
#include <iterator>

using namespace std;

class Apply{
public:
    int line_length;
    int current_size;

    Apply(int n) : line_length(n){
        current_size = 0;
    }

    //     Apply a(3);
    //     a("hello");  This allows us to call an instantiated object as a function, with given argument
    void operator() (const string& word){
        int size = word.length();
        if (current_size + size >= line_length){
            cout << "\n";
            current_size = 0;
        }
        cout << word << " ";
        current_size += size + 1;
    }
};

// This function compairs each map's key, value pair and returns the one with largest value
bool cmp(const pair<string, int>  &p1, const pair<string, int> &p2){
    return p1.second > p2.second;
}

// This function prints program usage as well as an error message when supplied
void print_usage(const string& command, const string& error = ""){
    cerr << "Usage: " << command << " file_name -OPTION\n";
    cerr << "Allowed options are:\n";
    cerr << "\ta - Alphabetical order\n";
    cerr << "\tf - Word occurrence order\n";
    cerr << "\tp N - Preserving file order. An additional parameter N must be supplied for the number of characters printed per line\n";
    cerr << error;
}

// This function
string word_cleaner(string word){
    string leading_junk = "\"'(";
    string trailing_junk = "!?;,:.\"')";
    stringstream ss;
    ss.str(word);
    size_t found;

    // Remove leading junk
    int count=0;
    for(char& c : word){
        found=trailing_junk.find(c);
        if(found == string::npos){
            break;
        }
        count++;
    }
    word.erase(0, count);

    // remove trailing junk
    count=0;
    for (int i = word.length()-1; i >= 0; i--){
        found=trailing_junk.find(word[i]);
        if(found == string::npos){
            break;
        }
        count++;
    }

    word.erase((word.length() - count), count);
    // Remove 's
    if (word.length() >= 2){
        if (word[word.length()-1] == 's' && word[word.length()-2] == '\''){
            word.erase(word.length()-2, word.length());
        }
    }
    return word;
}


bool check_if_valid(const string& clean_word){
    bool got_hyphen = false;
    for(const char& c : clean_word){
        if(!isalpha(c)){
            if(c == '-' && got_hyphen){
                return false;
            }else if(c == '-'){
                got_hyphen = true;
                continue;
            }else{
                return false;
            }
        }
        got_hyphen = false;
    }
    return clean_word.length() >= 3;
}

// Takes in a filename and reads every word of that file
// If a word is valid, put it into a vector
vector<string> parse_string(const string& filename){
    ifstream infile(filename);

    // Put this in a function and return vector (Why do you hate us?)
    string word;
    vector<string> word_list_vector;
    word_list_vector.clear();

    while (infile >> word){
        // Take junk from word
        string clean_word = word_cleaner(word);

        // Validate word
        bool valid = check_if_valid(clean_word);

        if (valid){
            // convert word to lower case
            transform(clean_word.begin(), clean_word.end(), clean_word.begin(), ::tolower);
            // push to vector
            word_list_vector.push_back(clean_word);
        }
    }
    return word_list_vector;
}

//Takes in a vector and converts it into a map with the frequency of occurrences
map<string,int> convert_to_map(const vector<string>& word_list_vector){
    map<string,int> word_list_map;

    for(int i = 0; i < word_list_vector.size(); ++i){
        string clean_word = word_list_vector[i];
        word_list_map[clean_word]++;
    }
    return word_list_map;
}

int main(int argc, char* argv[]){

    string filename;
    if (argc < 2){
        cout << "What's the filename: ";
        cin >> filename;
    }else{
        filename = argv[1];
    }

    if (argc < 3){
        print_usage(argv[0], "ERROR: Please specify an option for printing.\n");
        return 1;
    }
    string option = argv[2];
    if (option.length() != 2){
        print_usage(argv[0], "ERROR: Option should follow the following format \"-x\" where x is your option\n");
        return 1;
    }
    char opt = argv[2][1];

    // Get vector with words
    vector<string> word_list_vector = parse_string(filename);

    // Use max_element on vector to determine string with max length.
    int largest_string = max_element(word_list_vector.begin(), word_list_vector.end(),
                                         [](const string& a, const string& b){ return a.length() < b.length(); }) -> length();

    // Convert vector to map
    map<string,int> word_list_map = convert_to_map(word_list_vector);

    // Find number of occurrences of most frequent word
    auto pr = max_element(word_list_map.begin(), word_list_map.end(),
                               [](const pair<string, int>& p1, const pair<string, int>& p2){
                                   return p1.second < p2.second; });
    int most_occurred_frequency = pr -> second;
    // Calculate how many spaces we actually need
    int space_needed = to_string(most_occurred_frequency).length();

    int n;
    switch (opt){
        case 'a':
            for(auto it = word_list_map.cbegin(); it != word_list_map.cend(); ++it){
                cout << setw(largest_string) << left;
                cout << it->first << " " << setw(space_needed) << right << it->second << "\n";
            }
            break;
        case 'f':
            {
                vector<pair<string, int>> our_vector(word_list_map.begin(), word_list_map.end());
                sort(our_vector.begin(), our_vector.end(), cmp);
                for(int i = 0; i < our_vector.size(); ++i){
                    cout << setw(largest_string) << right << our_vector[i].first << " : ";
                    cout << setw(space_needed) << right << our_vector[i].second << endl;
                }
                break;
            }
        case 'o':
            if(argc < 4){
                print_usage(argv[0], "ERROR: You need to supply a maximum line size\n");
                return 1;
            }
            n = stoi(argv[3]);
            for_each (word_list_vector.begin(), word_list_vector.end(), Apply(n));
            cout << "\n";
            break;
        default:
            print_usage(argv[0], "ERROR: Unidentified option used.\n");
            return 1;
    }
    return 0;

}



