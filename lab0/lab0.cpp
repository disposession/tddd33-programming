#include <iostream>

using namespace std;


int main()
{
    char inchar;
    int inint;
    double indouble;
    string instring;

    cout << "Enter one integer: ";
    cin >> inint;
    cout << "You entered the number: " << inint << '\n';

    // CHECK!!
    // fix bug with inchar only taking one char
    cout << "Enter four integers: ";
    cin >> inint;
    cin.clear();
    indouble = inint;
    cin >> inint;
    cin.clear();
    inchar = '\0' + inint;
    cin >> inint >> instring;
    cout << "You entered the numbers: " << indouble << ' ' << ((int) inchar) << ' ' << inint << ' ' << instring << '\n';

    cout << "Enter one integer and one real number: ";
    cin >> inint >> indouble;
    std::cin.ignore(INT_MAX);
    cin.clear();
    cout << "The real is:\t" << indouble << '\n';
    cout << "The integer is:\t" << inint << '\n';

    cout << "Enter one real and one integer number: ";
    cin >> indouble >> inint;
    cout << "The real is: . . . . .  " << indouble << '\n';
    cout << "The integer is: . . . " << inint << '\n';

    cout << "Enter a character: ";
    cin >> inchar;
    cout << "You entered: " << inchar << '\n';

    cout << "Enter a word: ";
    cin >> instring;
    cout << "The word " << instring << " has " << instring.size() << " letters\n";

    cout << "Enter an integer and a word: ";
    cin >> inint >> instring;
    cout << "You entered '" << inint << "' and '" << instring << "'\n";

    cout << "Enter a character and a word: ";
    cin >> inchar >> instring;
    cout << "You entered the string " << instring << " and the character " << inchar;

    cout << "Enter a word and a real number: ";
    cin >> instring >> indouble;
    cout << "You entered " << instring << " and " << indouble;

    cout << "Enter a text-line: ";
    cin >> instring;
    cout << "You entered " << instring;

    cout << "Enter a second line of text: ";
    cin >> instring;
    cout << "You entered " << instring;

    cout << "Enter three words: ";
    cin >> instring;
    cout << "You entered " << instring;

    return 0;
}