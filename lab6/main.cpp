#include <iostream>
#include <iomanip>
#include <vector>
#include "component.h"
#include "component.cpp"


// This function prints program usage as well as an error message when supplied
void print_usage(const string& command, const string& error = "") {
    cerr << "Usage: " << command << " <number of iterations> <number of prints> <time step> <battery voltage>\n";
    cerr << error;
}


int main(int argc, char* argv[]) {
    // Make sure argc and argv are right. Otherwise, ask for it again
    int number_of_iterations, number_of_prints;
    double time_step, battery_voltage;

    if(argc < 4) {
        print_usage(argv[0], "You're missing some arguments");
    }

    try {
        number_of_iterations = stoi(argv[1]);
    }catch (const invalid_argument &) {
        print_usage(argv[0], "Number of iterations value is not an integer");
        return 1;
    }

    try {
        number_of_prints = stoi(argv[2]);
    }catch (const invalid_argument &) {
        print_usage(argv[0], "Number of prints value is not an integer");
        return 1;
    }

    try {
        time_step = stof(argv[3]);
    }catch (const invalid_argument &) {
        print_usage(argv[0], "time step is not a number");
        return 1;
    }

    try {
        battery_voltage = stof(argv[4]);
    }catch (const invalid_argument &) {
        print_usage(argv[0], "Battery voltade is not a number");
        return 1;
    }

    Wire* w1 = new Wire("wire1");
    Wire* w2 = new Wire("wire2");
    Wire* w3 = new Wire("wire3");
    Wire* w4 = new Wire("wire4");

    Net* net = new Net();

    /* // My circuit
    net->push(new Battery("b1", w1, w3, battery_voltage));
    net->push(new Capacitor("c1", w1, w2, 0.8));
    net->push(new Resistor("r1", w2, w3, 2.0));
    */


    // Circuit 1

    net->push(new Battery("b1", w1, w4, battery_voltage));
    net->push(new Resistor("r1", w1, w2, 6.0));
    net->push(new Resistor("r2", w2, w3, 4.0));
    net->push(new Resistor("r3", w3, w4, 8.0));
    net->push(new Resistor("r4", w2, w4, 12.0));
    /*
    // Circuit 2
    net->push(new Battery("b1", w1, w4, battery_voltage));
    net->push(new Resistor("r1", w1, w2, 150.0));
    net->push(new Resistor("r2", w1, w3, 50.0));
    net->push(new Resistor("r3", w2, w3, 100.0));
    net->push(new Resistor("r4", w2, w4, 300.0));
    net->push(new Resistor("r5", w3, w4, 250.0));


    // Circuit 3
    net->push(new Battery("b1", w1, w4, battery_voltage));
    net->push(new Resistor("r1", w1, w2, 150.0));
    net->push(new Resistor("r2", w1, w3, 50.0));
    net->push(new Capacitor("c3", w2, w3, 1.0));
    net->push(new Resistor("r4", w2, w4, 300.0));
    net->push(new Capacitor("c5", w3, w4, 0.75));
    */

    double steps_between_print = number_of_iterations / number_of_prints;
    int steps = 0;

    // Print header
    net->print_header();
    cout << "\n";
    for (unsigned int x=0; x<net->get_size(); x++) {
        cout << "Volt   Curr" << setw(15);
    }
    cout << "\n";
    cout << setprecision(2) << fixed;

    while (number_of_iterations > 0) {
        steps++;
        if (steps >= steps_between_print) {
            net->print_status();
            cout << "\n";
            steps = 0;
        }

        net->do_stuff(time_step);

        number_of_iterations--;
    }

    return 0;
}