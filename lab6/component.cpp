#include <iostream>
#include <iomanip>
#include <vector>
#include "component.h"

using namespace std;


// WIRE METHODS
Wire::Wire(string name) : name{name}, voltage{0} {};

void Wire::print_voltage() const {
    cout << "Wire's " << name << " is " << voltage << "\n";
}

void Wire::set_voltage(double value) {
    voltage = value;
}

double Wire::get_voltage() const {
    return voltage;
}

Wire* Component::get_positive_wire() const {
    return wire1;
}

Wire* Component::get_negative_wire() const {
    return wire2;
}


// COMPONENT METHODS
Component::Component(string name, Wire* w1, Wire* w2) : name{name}, wire1{w1}, wire2{w2} {};

double Component::get_voltage() const {
    return abs(wire1->get_voltage() - wire2->get_voltage());
}


// BATTERY METHODS
Battery::Battery(string name, Wire* w1, Wire* w2, double voltage): Component(name, w1, w2), voltage{voltage} {
    w2->set_voltage(0.0);
    w1->set_voltage(voltage);
};

void Battery::do_stuff(double ) {
    wire2->set_voltage(0.0);
    wire1->set_voltage(voltage);
    return ;
}

double Battery::get_current() const {
    return 0;
}


// CAPACITOR METHODS
Capacitor::Capacitor(string name, Wire* w1, Wire* w2, double capacitance): Component(name, w1, w2), capacitance{capacitance} {
    stored = 0;
};

void Capacitor::do_stuff(double time_step) {
    double w1_voltage = wire1->get_voltage();
    double w2_voltage = wire2->get_voltage();
    double difference = abs(w1_voltage - w2_voltage);
    double current_to_store_and_move = (difference - stored) * capacitance * time_step;

    stored+=current_to_store_and_move;

    if (w1_voltage > w2_voltage) {
        wire1->set_voltage(w1_voltage-current_to_store_and_move);
        wire2->set_voltage(w2_voltage+current_to_store_and_move);
    }else if (w1_voltage < w2_voltage) {
        wire1->set_voltage(w1_voltage+current_to_store_and_move);
        wire2->set_voltage(w2_voltage-current_to_store_and_move);
    }

    return ;
}


double Capacitor::get_current() const {
    return capacitance * (get_voltage() - stored);
}


// RESISTOR METHODS
Resistor::Resistor(string name, Wire* w1, Wire* w2, double resistance): Component(name, w1, w2), resistance{resistance} {};

void Resistor::do_stuff(double time_step) {
    double w1_voltage = wire1->get_voltage();
    double w2_voltage = wire2->get_voltage();
    double difference = abs(w1_voltage - w2_voltage);
    double current_to_move = difference / resistance * time_step;

    if (w1_voltage > w2_voltage) {
        wire1->set_voltage(w1_voltage-current_to_move);
        wire2->set_voltage(w2_voltage+current_to_move);
    }else if (w1_voltage < w2_voltage) {
        wire1->set_voltage(w1_voltage+current_to_move);
        wire2->set_voltage(w2_voltage-current_to_move);
    }
    return ;
}


double Resistor::get_current() const {
    return get_voltage() / resistance;
}


// NET METHODS
Net::Net(): size{0} {}

Net::~Net() {
    for (Component* c : net) {
        delete c;
    }
}

void Net::push(Component* comp) {
    net.push_back(comp);
    size++;
}

void Net::print_header() const {
    for (const Component* c : net) {
        cout << c->name << setw(15);
    }
}

void Net::print_status() const {
    for (Component* c : net) {
        cout << c->get_voltage() << setw(8) << c->get_current() << setw(7);
    }
}

void Net::do_stuff(double time_step) const {
    for (Component* c : net) {
        c->do_stuff(time_step);
    }
}

int Net::get_size() const {
    return size;
}