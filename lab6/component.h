#ifndef CPPTUT_COMPONENT_H
#define CPPTUT_COMPONENT_H

#include <cmath>
#include <stdexcept>
#include <string>

using namespace std;

class Wire {
public:
    //
    Wire(string name);

    Wire(const Wire &originalWire) = delete;
    Wire& operator=(const Wire &wire) = delete;

    string name;

    void print_voltage() const;

    void set_voltage(double value);

    double get_voltage() const;

private:
    double voltage;
};


class Component {
public:
    Component(string name, Wire* w1, Wire* w2);
    virtual ~Component() = default;
    Component(const Component &originalComp) = delete;
    Component& operator=(const Component &comp) = delete;

    string name;

    virtual void do_stuff(double time_step) = 0;
    virtual double get_current() const = 0;

    Wire* get_positive_wire() const;

    Wire* get_negative_wire() const;

    double get_voltage() const;

protected:
    Wire *wire1;
    Wire *wire2;
};


class Battery: public Component {
public:
    Battery(string name, Wire* w1, Wire* w2, double voltage);

    virtual void do_stuff(double);

    virtual double get_current() const;

private:
    double voltage;
};


class Capacitor: public Component {
public:
    Capacitor(string name, Wire* w1, Wire* w2, double capacitance);

    virtual void do_stuff(double time_step);

    virtual double get_current() const;

private:
    double capacitance;
    double stored;
};

class Resistor: public Component {
public:
    Resistor(string name, Wire* w1, Wire* w2, double resistance);

    virtual void do_stuff(double time_step);

    virtual double get_current() const;

private:
    double resistance;
};


class Net {
public:
    Net();
    ~Net();
    void push(Component* comp);
    void print_header() const;
    void print_status() const;
    void do_stuff(double time_step) const;
    int get_size() const;
private:
    vector<Component*> net;
    int size;
};

#endif //CPPTUT_COMPONENT_H
