#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

bool in_range(int value, int min, int max){
    return value <= max && value >= min;
}

string valid_trimino_piece(vector <int> piece, int min, int max){
    bool within_range;
    within_range = in_range(piece[0], min, max);
    within_range = in_range(piece[1], min, max) && within_range;
    within_range = in_range(piece[2], min, max) && within_range;
    if (!within_range) return "Values not within allowed range.";

    for (int i=0; i<3; i++){
        if ((piece[i] <= piece[i+1]) && (piece[(i+1) % 3] <= piece[(i+2) % 3])){
            return "";
        }
    }
    return "Values are non-compliant with game rules";
}

int parse_input(istream& infile, int min_range, int max_range){
//int parse_input(ifstream& infile, int min_range, int max_range){

    int line_number = 0;
    string s;
    // Get readin' !!
    while (getline(infile, s)){
        int a, b, c;
        stringstream stream_for_this_line;
        stream_for_this_line.str(s);
        string imagename;

        // New line
        line_number++;

        if (!(stream_for_this_line >> a)){
            cerr << "Error in line " << line_number << ". Could not read first piece number.\n";
            continue;
        }
        if (!(stream_for_this_line >> b)){
            cerr << "Error in line " << line_number << ". Could not read second piece number.\n";
            continue;
        }
        if (!(stream_for_this_line >> c)){
            cerr << "Error in line " << line_number << ". Could not read third piece number.\n";
            continue;
        }
        stream_for_this_line >> imagename;


        // push_back <- check docs
        vector <int> piece = {a, b, c};

        // Check if numbers are good
        string result = valid_trimino_piece(piece, min_range, max_range);
        if (!result.empty()){
            cerr << "Error at line " << line_number << ", piece is invalid: " << result << "\n";
            continue;
        }
    }
    return 0;
}


int main(int argc, char* argv[]){
    // Make sure argc and argv are right. Otherwise, ask for it again
    string filename;
    int min_range, max_range;
    if(argc < 4){
        cout << "You're missing some arguments\n";
    }

    if (argc==1){
        cout << "What's the filename: ";
        cin >> filename;
    }else{
        filename = argv[1];
    }

    if (argc<=2){
        cout << "What's the minimum range: ";
        cin >> min_range;
    }else{
        try {
            min_range = stoi(argv[2]);
        }catch (const invalid_argument &){
            cout << "min range is not an integer\n";
            return 1;
        }
    }

    if (argc<=3){
        cout << "What's the maximum range: ";
        cin >> max_range;
    }else{
        try {
            max_range = stoi(argv[3]);
        }catch (const invalid_argument &){
            cout << "max range is not an integer\n";
            return 1;
        }
    }

    if(filename.compare("-") == 0){
        return parse_input(cin, min_range, max_range);
    }else{
        try {
            ifstream infile(filename);
            return parse_input(infile, min_range, max_range);
            //return parse_input(cin, min_range, max_range);
        }catch (const ifstream::failure e){
            cout << "Could not open file!\n";
            return 1;
        }
    };
}