#include <iostream>
#include <algorithm>
#include <tuple>
#include "aabb.h"


using namespace std;

int round_to_nearest_int( float r ) {
    return (r > 0.0) ? (r + 0.5) : (r - 0.5);
}

// CHeck initialization lists
AABB::AABB( int top_parameter, int left_parameter, int bottom_parameter, int right_parameter) {
    int temp;
    if (left_parameter > right_parameter){
        temp = left_parameter;
        left_parameter = right_parameter;
        right_parameter = temp;
    }
    if (bottom_parameter < top_parameter){
        temp = bottom_parameter;
        bottom_parameter = top_parameter;
        top_parameter = temp;
    }
    top = top_parameter;
    left = left_parameter;
    bottom = bottom_parameter;
    right = right_parameter;
}

void AABB::print() {
    cout << "(" << left << ", " << top << ")\t";
    cout << "(" << right << ", " << bottom << ")\n";
}

void AABB::move(const int x, const int y){
    top += y;
    bottom += y;
    left += x;
    right += x;
}

bool AABB::contain(pt point) {
    return point.x >= left && point.x <= right && point.y >= top && point.y <= bottom;
}

bool AABB::contain(int x, int y) {
    return x >= left && x <= right && y >= top && y <= bottom;
}

bool AABB::intersect(AABB const box) const {
    if (box.left >= right) return false;
    if (box.right <= left) return false;
    if (box.bottom <= top) return false;
    if (box.top >= bottom) return false;
    return true;
}

AABB AABB::min_bounding_box(AABB const box) const {
    AABB newaabb(min(top, box.top), min(left, box.left), max(bottom, box.bottom), max(right, box.right));
    return newaabb;
}

bool AABB::may_collide(AABB first_position, AABB final_position){
    AABB possible_collision_area = first_position.min_bounding_box(final_position);
    return intersect(possible_collision_area);
}

bool AABB::collision_point(AABB first_position, AABB final_position, pt &output_parameter) {
    // What is the vector for this movement?
    int abscissa_distance = final_position.left - first_position.left;
    int ordinate_distance = final_position.bottom - first_position.bottom;

    // Will how aabb move 1 step in abscissa or ordinate?
    bool move_in_abscissa;

    // How many steps do we add on x and y at each iteration?
    int move_x;
    int move_y;

    // The slope of the linear function that will create the movement
    float slope = static_cast<float>(ordinate_distance)/static_cast<float>(abscissa_distance);

    // Make sure that the first_position is always the left most or up most depending on where we're going
    // Set up how we're going to move
    if (abs(abscissa_distance)>=abs(ordinate_distance)){
        move_in_abscissa = true;
        move_x = 1;
        move_y = static_cast<int>(slope);
        if (abscissa_distance < 0){
            swap(first_position, final_position);
        }
    }else{
        move_in_abscissa = false;
        int move_x = static_cast<int>(1.0/slope);
        int move_y = 1;
        if(ordinate_distance < 0){
            // switch them around
            swap(first_position, final_position);
        }
    }

    while ((first_position.left != final_position.left && move_in_abscissa) or (first_position.bottom != final_position.bottom && !move_in_abscissa)){
        // How far are we? idk man, pretty far I guess
        if (intersect(first_position)){
            // move back
            move(-move_x, -move_y);
            output_parameter.x = first_position.left;
            output_parameter.y = first_position.top;
            return true;
        }
        first_position.move(move_x, move_y);
	}
    output_parameter.x = -1;
    output_parameter.y = -1;
    return false;
}
