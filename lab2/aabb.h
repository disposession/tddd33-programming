#ifndef CPPTUT_AABB_H
#define CPPTUT_AABB_H

class pt{
public:
    int x;
    int y;

    pt(int x_param, int y_param){
        x = x_param;
        y = y_param;
    }

    pt(){
	x=0;
	y=0;
    };
};

// Our bound class
class AABB {
public:
    int top;
    int left;
    int bottom;
    int right;

    AABB( int top_parameter, int left_parameter, int bottom_parameter, int right_parameter);

    void print();
    void move(const int x, const int y);
    bool contain(pt point);
    bool contain(int x, int y);
    bool intersect(AABB const box) const;
    AABB min_bounding_box(AABB const box) const;
    bool may_collide(AABB first_position, AABB final_position);
    bool collision_point(AABB first_position, AABB final_position, pt &output_parameter);
};


#endif //CPPTUT_AABB_H



