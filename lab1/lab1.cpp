#include <iostream>
#include <iomanip>

using namespace std;


int main() {
    float first;
    float last;
    float stride;
    float taxp;
    bool sanitized_first = false;
    bool sanitized_last = false;
    bool sanitized_stride = false;
    bool sanitized_taxp = false;

    do{
        cout << "Enter first price: ";
        cin >> first;
        if (first < 0) {
            cout << "ERROR: First price must be at least 0 (ZERO) SEK\n";
        }else{
            sanitized_first = true;
        }
    }while (!sanitized_first);

    do{
        cout << left << setw(17) << "Enter last price" << ": ";
        cin >> last;
        if(last < first){
            cout << "ERROR: last price must be at least as large as the first price.\n";
        }else{
            sanitized_last = true;
        }
    }while (!sanitized_last);

    do{
        cout << left << setw(17) << "Enter stride" << ": ";
        cin >> stride;
        if (stride < 0.01f) {
            cout << "ERROR: Stride must be at least 0.01\n";
        }else{
            sanitized_stride = true;
        }
    }while (!sanitized_stride);

    do{
        cout << "Enter tax percent: ";
        cin >> taxp;
        if (taxp < 0 || taxp > 100) {
            cout << "ERROR: tax percent must be in the [0, 100] interval.\n";
        }else{
            sanitized_taxp = true;
        }
    }while (!sanitized_taxp);


    cout << right << setw(15)<<"Price"<<setw(12)<<"Tax"<<setw(25)<<"Price with tax"<<"\n";
    cout << setfill('-') << setw (53) << "\n";

    // Round up this number to the nearest integer.
    int number_of_iterations = ((last - first) / stride) + 1;
    for (int i=0; i<number_of_iterations; i++){
        float printing_value = first + (i * stride);
        float taxvalue = printing_value*(taxp/100.0);
        cout << setfill(' ') << fixed << setprecision(2);
        cout << setw(15) << printing_value << setw(12) << taxvalue;
        cout << setw(25) << printing_value + taxvalue << '\n';
    }
}