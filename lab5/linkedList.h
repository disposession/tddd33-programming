#ifndef CPPTUT_LINKEDLIST_H
#define CPPTUT_LINKEDLIST_H


class LinkedList{
public:
    // Constructor
    LinkedList();
    // Destructor
    ~LinkedList();
    // Copy constructor
    LinkedList(const LinkedList &orginalLL);
    // Assignment override
    LinkedList& operator=(const LinkedList &LL);
    // Move Constructor
    LinkedList(LinkedList &&orginalLL);
    // Move Assignment
    LinkedList& operator=(LinkedList &&LL);
    bool isEmpty() const;
    int getSize() const;
    double getElementAt(int position) const;
    void print() const;
    void insert(double value_to_add);
    bool remove(double value_to_remove);

private:

    class Node{
    public:
        double value;
        Node *next;
    };

    int size;
    Node *first;

    void theInsertingSpot(double value_to_add, Node& previous, Node& next);
};


#endif //CPPTUT_LINKEDLIST_H
