// This test program uses a test framework supporting TDD and BDD.
// You are not required to use the framework, but encouraged to.
// Code:
// https://github.com/philsquared/Catch.git
// Documentation:
// https://github.com/philsquared/Catch/blob/master/docs/tutorial.md

// You ARE however required to implement all test cases outlined here,
// even if you do it by way of your own function for each case.  You
// are recommended to solve the cases in order, and rerun all tests
// after you modify your code.

// This define lets Catch create the main test program
// (Must be in only one place!)
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "linkedList.h"
#include <random>

//=======================================================================
// Test cases
//=======================================================================

LinkedList trigger_move(LinkedList l) {
    // do some (any) modification to list
    l.insert(30);
    return l;
}


SCENARIO( "Empty lists" ) {

    GIVEN( "An empty list" ) {
        LinkedList l{};

        REQUIRE( l.isEmpty() );
        REQUIRE( l.getSize() == 0 );

        WHEN( "an item is inserted" ) {

            // insert an item
            l.insert(5);

            THEN( "the size increase and the item is first in the list" ) {
                REQUIRE( l.getSize() == 1 );
                REQUIRE( l.getElementAt(0) == 5 );
            }
        }

        WHEN( "an item is removed" ) {

            // remove an item
            l.remove(5);

            THEN( "the list is still empty" ) {
                // add your REQUIRE statements
                REQUIRE( l.isEmpty() );
                REQUIRE( l.getSize() == 0 );
            }
        }

        WHEN( "the list is copied to a new list" ) {

            // copy your list to a new variable (copy constructor)
            LinkedList c;
            c = l;

            THEN( "the new list is also empty" ) {
                // add your REQUIRE statements
                REQUIRE( c.getSize() == 0 );
                REQUIRE( c.isEmpty() );
            }
        }


        WHEN( "the list is copied to an existing non-empty list" ) {
            // create and fill a list to act as the existing list
            // copy (assign) your empty list to the existing
            LinkedList c{};
            c.insert(1);
            c = l;
            // Remove something on the original
            //l.remove(5);

            THEN( "The copy should be as the original before being altered" ) {
                // add your REQUIRE statements
                REQUIRE( c.isEmpty() );
                REQUIRE( c.getSize() == 0 );
            }

        }
    }
}

SCENARIO( "Single item lists" ) {

    GIVEN( "A list with one item in it" ) {

        // create the given scenario
        LinkedList l{};
        l.insert(5);

        REQUIRE( l.getSize() == 1 );
        REQUIRE( l.getElementAt(0) == 5);

        WHEN( "a smaller item is inserted" ) {
            l.insert(2);
            THEN( /* describe what will happen */ ) {
                REQUIRE( l.getSize() == 2 );
                REQUIRE( l.getElementAt(0) == 2 );
                REQUIRE( l.getElementAt(1) == 5 );
            }
        }
        WHEN( "a larger item is inserted" ) {
            l.insert(7);
            THEN( /* describe what will happen */ ) {
                REQUIRE( l.getSize() == 2 );
                REQUIRE( l.getElementAt(0) == 5 );
                REQUIRE( l.getElementAt(1) == 7 );
            }
        }
        WHEN( "an item is removed" ) {
            l.remove(5);
            THEN( /* describe what will happen */ ) {
                REQUIRE( l.isEmpty() );
            }
        }
        WHEN( "an item that doesn't exist is removed" ) {
            l.remove(4);
            THEN( /* describe what will happen */ ) {
                REQUIRE( l.getSize() == 1 );
                REQUIRE( l.getElementAt(0) == 5 );
            }
        }
        WHEN( "the list is copied to a new list" ) {
            LinkedList c;
            c = l;
            THEN( /* describe what will happen */ ) {
                REQUIRE( c.getSize() == 1 );
                REQUIRE( c.getElementAt(0) == 5 );
            }
        }
        WHEN( "the list is copied to an existing non-empty list" ) {
            LinkedList c{};
            c.insert(1);
            c = l;
            THEN( /* describe what will happen */ ) {
                REQUIRE( c.getSize() == 1 );
                REQUIRE( c.getElementAt(0) == 5 );
            }
        }
    }
}

SCENARIO( "Multi-item lists" ) {
    LinkedList l{};
    l.insert(6);
    l.insert(10);
    l.insert(8);
    l.insert(2);
    l.insert(4);

    REQUIRE( l.getSize() == 5 );
    REQUIRE( l.getElementAt(0) == 2 );
    REQUIRE( l.getElementAt(1) == 4 );
    REQUIRE( l.getElementAt(2) == 6 );
    REQUIRE( l.getElementAt(3) == 8 );
    REQUIRE( l.getElementAt(4) == 10 );

    GIVEN( "A list with five items in it" ) {

        // create the given scenario and all THEN statements
        // and all REQUIRE statements

        WHEN( "an item smaller than all other is inserted" ) {
            l.insert(0);
            THEN( /* describe what will happen */ ) {
                REQUIRE( l.getSize() == 6 );
                REQUIRE( l.getElementAt(0) == 0 );
                REQUIRE( l.getElementAt(1) == 2 );
                REQUIRE( l.getElementAt(2) == 4 );
                REQUIRE( l.getElementAt(3) == 6 );
                REQUIRE( l.getElementAt(4) == 8 );
                REQUIRE( l.getElementAt(5) == 10 );
            }
        }
        WHEN( "an item larger than all other is inserted" ) {
            l.insert(12);
            THEN( /* describe what will happen */ ) {
                REQUIRE( l.getSize() == 6 );
                REQUIRE( l.getElementAt(0) == 2 );
                REQUIRE( l.getElementAt(1) == 4 );
                REQUIRE( l.getElementAt(2) == 6 );
                REQUIRE( l.getElementAt(3) == 8 );
                REQUIRE( l.getElementAt(4) == 10 );
                REQUIRE( l.getElementAt(5) == 12 );
            }
        }
        WHEN( "an item smaller than all but one item is inserted" ) {
            l.insert(3);
            THEN( /* describe what will happen */ ) {
                REQUIRE( l.getSize() == 6 );
                REQUIRE( l.getElementAt(0) == 2 );
                REQUIRE( l.getElementAt(1) == 3 );
                REQUIRE( l.getElementAt(2) == 4 );
                REQUIRE( l.getElementAt(3) == 6 );
                REQUIRE( l.getElementAt(4) == 8 );
                REQUIRE( l.getElementAt(5) == 10 );
            }
        }
        WHEN( "an item larger than all but one item is inserted" ) {
            l.insert(9);
            THEN( /* describe what will happen */ ) {
                REQUIRE( l.getSize() == 6 );
                REQUIRE( l.getElementAt(0) == 2 );
                REQUIRE( l.getElementAt(1) == 4 );
                REQUIRE( l.getElementAt(2) == 6 );
                REQUIRE( l.getElementAt(3) == 8 );
                REQUIRE( l.getElementAt(4) == 9 );
                REQUIRE( l.getElementAt(5) == 10 );
            }
        }
        WHEN( "an item is removed" ) {
            l.remove(6);
            THEN( /* describe what will happen */ ) {
                REQUIRE( l.getSize() == 4 );
                REQUIRE( l.getElementAt(0) == 2 );
                REQUIRE( l.getElementAt(1) == 4 );
                REQUIRE( l.getElementAt(2) == 8 );
                REQUIRE( l.getElementAt(3) == 10 );
            }
        }
        WHEN( "an item that doesn't exist is removed" ) {
            l.remove(42);
            THEN( /* describe what will happen */ ) {
                REQUIRE( l.getSize() == 5 );
                REQUIRE( l.getElementAt(0) == 2 );
                REQUIRE( l.getElementAt(1) == 4 );
                REQUIRE( l.getElementAt(2) == 6 );
                REQUIRE( l.getElementAt(3) == 8 );
                REQUIRE( l.getElementAt(4) == 10 );
            }
        }
        WHEN( "all items are removed" ) {
            l.remove(2);
            l.remove(4);
            l.remove(6);
            l.remove(8);
            l.remove(10);
            THEN( /* describe what will happen */  ) {
                REQUIRE( l.isEmpty() );
            }
        }
        WHEN( "the list is copied to a new list" ) {
            LinkedList c{};
            c = l;
            THEN( /* describe what will happen */  ) {
                REQUIRE( c.getSize() == 5 );
                REQUIRE( c.getElementAt(0) == 2 );
                REQUIRE( c.getElementAt(1) == 4 );
                REQUIRE( c.getElementAt(2) == 6 );
                REQUIRE( c.getElementAt(3) == 8 );
                REQUIRE( c.getElementAt(4) == 10 );
            }}
        WHEN( "the list is copied to an existing non-empty list" ) {
            LinkedList c{};
            c.insert(1);
            c.insert(3);
            c = l;
            THEN( /* describe what will happen */  ) {
                REQUIRE( c.getSize() == 5 );
                REQUIRE( c.getElementAt(0) == 2 );
                REQUIRE( c.getElementAt(1) == 4 );
                REQUIRE( c.getElementAt(2) == 6 );
                REQUIRE( c.getElementAt(3) == 8 );
                REQUIRE( c.getElementAt(4) == 10 );
            }
        }
        WHEN( "the list is copied to a new list and the original is changed" ) {
            LinkedList c{};
            c = l;
            l.remove(4);
            THEN( /* describe what will happen */  ) {
                REQUIRE( c.getSize() == 5 );
                REQUIRE( c.getElementAt(0) == 2 );
                REQUIRE( c.getElementAt(1) == 4 );
                REQUIRE( c.getElementAt(2) == 6 );
                REQUIRE( c.getElementAt(3) == 8 );
                REQUIRE( c.getElementAt(4) == 10 );
            }
        }
        WHEN( "the list is copied to a new list and the copy is changed" ) {
            LinkedList c{};
            c = l;
            c.remove(4);
            THEN( /* describe what will happen */  ) {
                REQUIRE( l.getSize() == 5 );
                REQUIRE( l.getElementAt(0) == 2 );
                REQUIRE( l.getElementAt(1) == 4 );
                REQUIRE( l.getElementAt(2) == 6 );
                REQUIRE( l.getElementAt(3) == 8 );
                REQUIRE( l.getElementAt(4) == 10 );
            }
        }
    }
}


SCENARIO( "Lists can be passed to functions" ) {

    GIVEN( "A list with 5 items in it" ) {

        LinkedList l{};
        l.insert(6);
        l.insert(10);
        l.insert(8);
        l.insert(2);
        l.insert(4);

        REQUIRE( l.getSize() == 5 );
        REQUIRE( l.getElementAt(0) == 2 );
        REQUIRE( l.getElementAt(1) == 4 );
        REQUIRE( l.getElementAt(2) == 6 );
        REQUIRE( l.getElementAt(3) == 8 );
        REQUIRE( l.getElementAt(4) == 10 );

        // insert 5 items

        WHEN( "the list is passed to trigger_move()" ) {

            // Change list inside trigger move and check if changes are applied ONLY on new LL.
            LinkedList s{ trigger_move(l) };

            THEN( "the given list remain and the result have the change" ) {
                REQUIRE( s.getSize() == 6 );
                REQUIRE( s.getElementAt(0) == 2 );
                REQUIRE( s.getElementAt(1) == 4 );
                REQUIRE( s.getElementAt(2) == 6 );
                REQUIRE( s.getElementAt(3) == 8 );
                REQUIRE( s.getElementAt(4) == 10 );
                REQUIRE( s.getElementAt(5) == 30 );

                REQUIRE( l.getSize() == 5 );
                REQUIRE( l.getElementAt(0) == 2 );
                REQUIRE( l.getElementAt(1) == 4 );
                REQUIRE( l.getElementAt(2) == 6 );
                REQUIRE( l.getElementAt(3) == 8 );
                REQUIRE( l.getElementAt(4) == 10 );
            }
        }
    }
}