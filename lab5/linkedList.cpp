#include <iostream>
#include "linkedList.h"

using namespace std;

// Constructor
LinkedList::LinkedList() {
    size = 0;
    first = nullptr;
}

// Destructor
LinkedList::~LinkedList() {
    Node* current = first;
    if (isEmpty()) {
        return;
    }else{
        Node* previous = first;
        while(current->next != nullptr) {
            previous = current;
            current = current->next;
            delete previous;
        }
    }
    delete current;
}

// Copy Constructor
LinkedList::LinkedList(const LinkedList &originalLL) {
    // do deep copying stuff
    size = originalLL.size;
    first = nullptr;

    if (!originalLL.isEmpty()) {
        // Copy the first node
        first = new Node;
        first->value = originalLL.first->value;

        // Travel through LL (both original and copy)
        Node* current_original = originalLL.first;
        Node* current_copy = first;
        while(current_original->next != nullptr) {
            Node* new_node = new Node;
            new_node->value = current_original->next->value;
            current_copy->next = new_node;

            current_original = current_original->next;
            current_copy = current_copy->next;
        }
        current_copy->next = nullptr;
    }
}

// Assignment Override
LinkedList& LinkedList::operator=(const LinkedList &LL) {
    LinkedList tmp{LL};
    swap(first, tmp.first);
    swap(size, tmp.size);

    return *this;
}

// Move Constructor
LinkedList::LinkedList(LinkedList &&originalLL) {
    // This basically means there's a xvalue as the argument. So let's use it as it will be obliterated
    size = originalLL.size;
    first = originalLL.first;
    originalLL.first = nullptr;
    originalLL.size = 0;
}

// Move Assignment
LinkedList& LinkedList::operator=(LinkedList &&originalLL) {
    swap(size, originalLL.size);
    swap(first, originalLL.first);

    return *this;
}

void LinkedList::print() const{
    Node* current = first;
    cout << current->value << "\n";
    while(current->next != nullptr) {
        cout << current->next->value << "\n";
        current = current->next;
    }
}

int LinkedList::getSize() const{
    return size;
}

bool LinkedList::isEmpty() const{
    return first == nullptr;
}

double LinkedList::getElementAt(int position) const{
    Node* current = first;
    while (position!=0) {
        current = current->next;
        position-=1;
    }
    return current->value;
}

void LinkedList::theInsertingSpot(double value_to_add, Node& previous, Node& current) {
    if (current.value > value_to_add) {
        // We found our spot! Let's make a settlement here. It is essential we gain control of this memory area
        // Create new node
        Node* new_node = new Node;
        new_node->value = value_to_add;

        // Build me trade routes with our neighboring towns. We need more supplies!
        previous.next = new_node;
        new_node->next = &current;
        // print comic relief
    }else if(current.next == nullptr) {
        //This long journey just brought us to the end of the list. You know what men? Just leave it here
        // Rally your forces, build me a node
        Node* new_node = new Node;
        new_node->value = value_to_add;

        // Good, brute patch it to the LL, anyone got duct tape?
        current.next = new_node;
        new_node->next = nullptr;

        // uh, sir. what about the size of the list? doesn't match now.
        // Soldier, you dumb? when all this recursion thing finishes our main battalion will still execute some code.
    }
    else{
        // This spot has no geostrategical advantage of any kind. Let's delve deeper into the valley
        theInsertingSpot(value_to_add, current, *current.next);
    }
};

void LinkedList::insert(double value_to_add) {
    // Insert to end of chain
    Node* current = first;
    Node* previous = first;
    // If the list is empty or has only one element, it kinda messes our plans for recursion, so let's handle those here
    if (isEmpty()) {
        first = new Node;
        first->next = nullptr;
        first->value = value_to_add;
    }else{
        if(first->value > value_to_add) {
            Node* temp = first;

            first = new Node;
            first->next = temp;
            first->value = value_to_add;
        }else{
            theInsertingSpot(value_to_add, *first, *first);
        }
    }
    // In either case, the size definitely increased. Let's take care of that
    size++;
}

bool LinkedList::remove(double value_to_remove) {
    // Insert to end of chain
    Node* current = first;
    if (isEmpty()) {
        return false;
    }else{
        Node* previous = first;
        if (current->value == value_to_remove) {
            // This is the first element of the list
            first = current->next;
            delete current;
            size=size-1;
            return true;
        }
        while(current->next != nullptr) {
            if (current->value == value_to_remove) {
                previous->next = current->next;
                delete current;
                size=size-1;
                return true;
            }
            previous = current;
            current = current->next;
        }
        return false;
    }
}

/*int main() {
    // First test initialize Linked List
    LinkedList* Jaraxxus_illidan_lord_of_the_burning_legion = new LinkedList();
    cout << "List initialized as empty, size should be 0: " << (Jaraxxus_illidan_lord_of_the_burning_legion->getSize()==0) << '\n';

    // Test Insert function

    Jaraxxus_illidan_lord_of_the_burning_legion->insert(5.0);
    //cout << (Jaraxxus_illidan_lord_of_the_burning_legion->getElementAt(0));
    Jaraxxus_illidan_lord_of_the_burning_legion->insert(3.0);
    Jaraxxus_illidan_lord_of_the_burning_legion->insert(9.0);
    Jaraxxus_illidan_lord_of_the_burning_legion->insert(7.0);

    cout << "4 elements added\n";
    cout << "LL size should be 4: " << (Jaraxxus_illidan_lord_of_the_burning_legion->getSize()==4) << '\n';
    cout << "First element should be 3: " << (Jaraxxus_illidan_lord_of_the_burning_legion->getElementAt(0)==3) << '\n';
    cout << "Last element should be 9: " << (Jaraxxus_illidan_lord_of_the_burning_legion->getElementAt(3)==9) << '\n';

    //(*Jaraxxus_illidan_lord_of_the_burning_legion).insert(3.0);
    //(*Jaraxxus_illidan_lord_of_the_burning_legion).print();
}*/